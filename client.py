import select
import socket


def main():
    RECV_BUFFER = 4096
    PORT = 1337
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setblocking(0)
    s.settimeout(None)
    s.connect(("127.0.0.1", PORT))
    s.send("test".encode("ascii"))
    while 1:
        try:
            data = s.recv(RECV_BUFFER)
            print(data.decode("ascii"))
        except socket.error as e:
            continue


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
